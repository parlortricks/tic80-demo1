# demo1

## Summary
Palette scrolled of defined palettes take from lospec that creates a scrolling rainbow effect to view each palette

## License
[SPDX-License-Identifier: GPL-3.0-or-later](https://spdx.org/licenses/GPL-3.0-or-later.html)

Copyright (c) 2021 parlortricks