(fn rainbow []
 (for [x 0 240]
  (for [y 0 136]
   (pix x y (/ (+ x y t) 8)))))
            
(fn handle-input []
 (when (btn 4) (set pause true))
 (when (btn 5) (set pause false)))
 
(fn _G.TIC []
 (handle-input)
 (load-palette pal)
 (cls 0)
 (rainbow)
  
 (rect 0 129 240 10 0)
 (print (. PALETTES pal) 1 130 1)
 (if (= false pause)
  (set t (+ t 1)))
        
 (if (= 0 (% t 120))
  (set pal (+ pal 2)))
        
 (if (> pal (# PALETTES))
  (set pal 1))
)
