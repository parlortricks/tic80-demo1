;; title:   demo1
;; author:  parlortricks
;; desc:    rainbow scroller of defined-palettes
;; website: https://gitlab.com/tic_80/demo1
;; script:  fennel
;; SPDX-License-Identifier: GPL-3.0-or-later
;; Copyright (c) 2021 parlortricks

(var t 0)
(var pal 1)
(var pause false)
